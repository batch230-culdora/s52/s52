// remove: import logo from './logo.svg';
import './App.css';
import { Container } from 'react-bootstrap';
import { Fragment } from 'react'; /* React Fragments allows us to return multiple elements*/

import AppNavbar from './components/AppNavbar';
// import Home from './pages/Home';
// import Courses from './pages/Courses'
/*import Register from './pages/Register'*/
import Login from './pages/Login'

function App() {
  return (
    /* React Fragments allows us to return multiple elements*/
    <Fragment>
      <AppNavbar />
      <Container>
      {/*<Register />*/}
      <Login />
        {/*<Home />
        <Courses />*/}
      </Container>
    </ Fragment>
  );
}

export default App;
