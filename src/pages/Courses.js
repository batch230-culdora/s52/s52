import { Fragment } from 'react';
import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';

export default function Courses(){

	console.log("Contents of coursesData: ");
	console.log(coursesData);
	console.log(coursesData[0]);

	// Array methods to display all courses
	// let count = 0;
	const courses = coursesData.map(course => {
		return (
			<CourseCard key ={course.id} courseProps = {course}/>
			)
	})

	/*return(
		<Fragment>
			<CourseCard courseProps={coursesData[0]} />
		</Fragment>
	)*/

	return (
			<Fragment>
				{courses}
			</Fragment>
		)
}


