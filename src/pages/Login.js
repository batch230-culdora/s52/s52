import { Form, Button } from 'react-bootstrap';
import { useState, useEffect } from 'react';

export default function Login(){


	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [isActive, setIsActive] = useState(false);

	// Check if values are successfully binded
	console.log(email);
	console.log(password1);
	

	function Login(event){
		event.preventDefault();

		// clear input fields
		setEmail('');
		setPassword1('');
		alert(`Welcome ${email}!`);
	}

	useEffect(()=>{
		if(email !== '' && password1 !== ''){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [email, password1])


	return(
		 <div className="container-fluid">
            <div className="row">
                <div className="col-md-4 col-sm-6 col-xs-12">
                   	<Form onSubmit = {(event) => Login(event)}>
						<h3>Login</h3>
            				<Form.Group controlId="userEmail">
               					 <Form.Label>Email address</Form.Label>
               						 <Form.Control 
	                							type="email" 
	                							placeholder="Enter email" 
	                							value = {email}
	                							onChange = {event => setEmail(event.target.value)}
	                							required
                							/>
            							</Form.Group>

            							<Form.Group controlId="password1">
                							<Form.Label>Password</Form.Label>
                							<							Form.Control 
	                							type="password" 
	                							placeholder="Password" 
	                							value = {password1}
	                							onChange = {event => setPassword1(event.target.value)}
	                							required
                							/>
            							</Form.Group>

            							<br/>
            							{ isActive ? // true
            							<Button variant="primary" type="submit" id="submitBtn">
            								Login
            							</Button>
            							: // false output
            							<Button variant="primary" type="submit" id="submitBtn" disabled>
            								Login
            							</Button>
        								}
        							</Form>
        </div>
      </div>
    </div>
	)
}