import { Form, Button } from 'react-bootstrap';
import { useState, useEffect } from 'react';

export default function Register(){

	//State hooks to store the values of input fields
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	// Check if values are successfully binded
	console.log(email);
	console.log(password1);
	console.log(password2);

	function registerUser(event){
		event.preventDefault();

		// clear input fields
		setEmail('');
		setPassword1('');
		setPassword2('');
		alert('Thank you for registering!');
	}

	useEffect(()=>{
		if((email !== '' && password1 !== '' &&password2 !== '') && (password1 === password2)){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [email, password1, password2])


	return(
		<Form onSubmit = {(event) => registerUser(event)}>
		<h3>Register</h3>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter email" 
	                value = {email}
	                onChange = {event => setEmail(event.target.value)}
	                required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Password" 
	                value = {password1}
	                onChange = {event => setPassword1(event.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Verify Password"
	                value = {password2}
	                onChange = {event => setPassword2(event.target.value)} 
	                required
                />
            </Form.Group><br/>
            { isActive ? // true
            <Button variant="primary" type="submit" id="submitBtn">
            	Submit
            </Button>
            : // false output
            <Button variant="primary" type="submit" id="submitBtn" disabled>
            	Submit
            </Button>
        	}
        </Form>

	)
}